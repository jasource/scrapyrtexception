class MyCustomSpiderMiddleware:
    def process_spider_exception(self, response, exception, spider):
        with open("error.txt", "a") as f:
            f.write("process_spider_exception called with the below exception\n" + str(exception) + "\n")
        print("######## process_spider_exception", exception)

        return None

class ExceptionMiddleware:
    def process_spider_input(self, response, spider):
        with open("error.txt", "a") as f:
            f.write("process_spider_input called and will throw an exception\n")
        raise Exception("####### From ExceptionMiddleware process_spider_input")
