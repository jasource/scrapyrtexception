# -*- coding: utf-8 -*-
BOT_NAME = 'quotesbot'

SPIDER_MODULES = ['quotesbot.spiders']
NEWSPIDER_MODULE = 'quotesbot.spiders'

ROBOTSTXT_OBEY = True

SPIDER_MIDDLEWARES = {
    'quotesbot.middlewares.ExceptionMiddleware': 544,
    'quotesbot.middlewares.MyCustomSpiderMiddleware': 543,
}
