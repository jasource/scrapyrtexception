First run `. ./setup.sh` to create a virtual environment and run


In another window:
Fire off a request to scrapyrt
```
(.venv) quotesbot % curl "localhost:9080/crawl.json?spider_name=toscrape-css&url=http://quotes.toscrape.com/"
```

The file `error.txt` should now be created and show that process_spider_input was called
```
(.venv) quotesbot % cat error.txt
process_spider_input called and will throw an exception
process_spider_exception called with the below exception
'Failure' object has no attribute 'css'
process_spider_exception called with the below exception
'Failure' object has no attribute 'css'
process_spider_exception called with the below exception
'Failure' object has no attribute 'css'
process_spider_exception called with the below exception
'Failure' object has no attribute 'css'
```

This shows process_spider_input was called and threw an exception, however the exception was not caught and sent to `process_spider_exception` like it should have been.  Later the "'Failure' object has no attribute 'css'" comes from an exception that is thrown when the parse method of


If you run a normal crawl:
```
(.venv) quotesbot % scrapy crawl toscrape-css
```

In the output you can also see that the `process_spider_input` throwing an error sends the output to the `process_spider_exception` method in the middleware.  This does not happen when running under `scrapyrt`.
```
...
######## process_spider_exception ####### From ExceptionMiddleware process_spider_input
2024-01-10 15:27:45 [scrapy.core.scraper] ERROR: Spider error processing <GET http://quotes.toscrape.com/> (referer: None)
Traceback (most recent call last):
  File "/Users/jonathan.alvarado/src/scrapyrtclean/quotesbot/.venv/lib/python3.11/site-packages/scrapy/core/spidermw.py", line 83, in _process_spider_input
    result = method(response=response, spider=spider)
             ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
  File "/Users/jonathan.alvarado/src/scrapyrtclean/quotesbot/quotesbot/middlewares.py", line 13, in process_spider_input
    raise Exception("####### From ExceptionMiddleware process_spider_input")
Exception: ####### From ExceptionMiddleware process_spider_input
...
```
